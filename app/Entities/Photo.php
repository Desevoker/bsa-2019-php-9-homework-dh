<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Carbon;

class Photo extends Model
{
    protected $attributes = [
        'status' => 'UPLOADED',
    ];

    protected $fillable = [
        'user_id',
        'original_photo',
        'photo_100_100',
        'photo_150_150',
        'photo_250_250',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status(): string
    {
        return strtolower($this->status);
    }

    public function processing()
    {
        $this->status = 'PROCESSING';

        return $this;
    }

    public function success()
    {
        $this->status = 'SUCCESS';

        return $this;
    }

    public function fail()
    {
        $this->status = 'FAIL';

        return $this;
    }

    public function timestamp(): string
    {
        return Carbon::parse($this->updated_at)->format('dmYHis');
    }

    public function url100x100(): string
    {
        return $this->url('photo_100_100');
    }

    public function url150x150(): string
    {
        return $this->url('photo_150_150');
    }

    public function url250x250(): string
    {
        return $this->url('photo_250_250');
    }

    protected function url(string $photoType): string
    {
        return Storage::url($this->$photoType);
    }
}
