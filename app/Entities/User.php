<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function imagesFolderPath(): string
    {
        return "images/{$this->id}";
    }

    public function createNewPhoto(UploadedFile $file): Photo
    {
        $this->clearImagesFolder();

        $path = $this->saveOriginalImageFile($file);

        return $this->photos()->create([
            'original_photo' => $path,
        ]);
    }

    protected function saveOriginalImageFile(UploadedFile $file): string
    {
        return Storage::putFileAs(
            $this->imagesFolderPath(),
            $file,
            'photo.' . $file->extension()
        );
    }

    protected function clearImagesFolder(): bool
    {
        return Storage::delete(
            Storage::files(
                $this->imagesFolderPath()
            )
        );
    }
}
