<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Contracts\PhotoService;
use App\Entities\Photo;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;

class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    public $photo;
    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
        $this->user = $photo->user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PhotoService $photoService)
    {
        $this->photo->processing()->save();

        $croppingSizes = [
            'photo_100_100' => ['width' => 100, 'height' => 100],
            'photo_150_150' => ['width' => 150, 'height' => 150],
            'photo_250_250' => ['width' => 250, 'height' => 250],
        ];

        $croppedImagesPaths = [];

        foreach ($croppingSizes as $type => $size) {
            $croppedImagesPaths[$type] = $photoService->moveFile(
                $photoService->crop(
                    $this->photo->original_photo,
                    $size['width'],
                    $size['height']
                ),
                $this->user->imagesFolderPath(),
                "photo{$size['width']}x{$size['height']}"
            );
        }

        $this->photo->fill($croppedImagesPaths)->success()->save();

        $this->user->notify(new ImageProcessedNotification($this->photo));
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        $this->photo->fail()->save();

        $this->user->notify(new ImageProcessingFailedNotification($this->photo));
    }
}
