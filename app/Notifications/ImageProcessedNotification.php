<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
use App\Entities\Photo;

class ImageProcessedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $photo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'mail'];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'status' => $this->photo->status(),
            'photo_100_100' => $this->photo->url100x100(),
            'photo_150_150' => $this->photo->url150x150(),
            'photo_250_250' => $this->photo->url250x250(),
            'timestamp' => $this->photo->timestamp(),
        ]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
                    ->line("Dear {$notifiable->name},")
                    ->line('Photos have been successfully uploaded and processed.')
                    ->line('Here are links to the images:')
                    ->line(url('/') . $this->photo->url100x100())
                    ->line(url('/') . $this->photo->url150x150())
                    ->line(url('/') . $this->photo->url250x250())
                    ->line('Thanks!');
    }
}
