<?php

namespace App\Services;

use App\Services\Contracts\PhotoService as IPhotoService;
use Illuminate\Contracts\Filesystem\Factory as Filesystem;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PhotoService implements IPhotoService
{
    private $fileSystem;

    public function __construct(Filesystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;
    }

    public function crop(string $pathToPhoto, int $width, int $height): string
    {
        $croppedImage = Image::make(
            Storage::path($pathToPhoto)
        )->crop($width, $height);

        $filePath = 'cropped/'
            . Str::random(40)
            . '.'
            . $croppedImage->extension;

        $this->fileSystem->put(
            $filePath,
            $croppedImage->stream()
        );

        return $filePath;
    }

    public function moveFile(string $file, string $toFolder, string $name): string
    {
        $path = $toFolder . "/{$name}." . File::extension($file);

        if (Storage::exists($path)) {
            Storage::delete($path);
        }

        Storage::move($file, $path);

        return $path;
    }
}
