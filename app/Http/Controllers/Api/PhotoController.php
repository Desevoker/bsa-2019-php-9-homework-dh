<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\CropJob;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest();

        $user = auth()->user();

        $file = $request->file('photo');

        $photo = $user->createNewPhoto($file);

        CropJob::dispatch($photo);
    }

    protected function validateRequest()
    {
        request()->validate([
            'photo' => ['required', 'image'],
        ]);
    }
}
